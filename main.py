import os
import asyncio
import discord
import threading
from discord.ext import commands
from dotenv import load_dotenv
from src.api.api import run_flask, setBot

load_dotenv(dotenv_path="config/token")

intents = discord.Intents().all()
help_command = commands.DefaultHelpCommand(no_category='Commandes')
bot = commands.Bot(command_prefix="<", help_command=help_command, intents=intents, description="Le larbin du serveur GBH.")
tree = bot.tree

# ce truc est juste magique
async def load_extensions():
    for extension in ['src.cogs.events', 'src.cogs.commands', 'src.cogs.tasks']:
        await bot.load_extension(extension)

async def run_bot():
    await load_extensions()
    await bot.start(os.getenv("TOKEN"))

if __name__ == "__main__":
    setBot(bot)
    threadAPI = threading.Thread(target=run_flask, daemon=True)
    threadAPI.start()
    asyncio.run(run_bot())
