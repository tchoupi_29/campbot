# CampBot

Un petit bot discord qui gère l'envoie de messages en mp.

## Installation

- `git clone https://gitlab.com/tchoupi_29/campbot.git`
- ajoutez le fichier `config/token` contenant le token du bot à la racine du projet avecle contenu `TOKEN=change-me`

## Utilisation

*Après activation de l'environement python* `python main.py`

## Fonctionnalités
### Commandes de CampBot

Le préfixe des commandes de CampBot est : `<`.

- ### `appelerLesTroupes`
    CampBot va envoyer une invitation à tous les participants qui veulent être spammés quand un scénario commence..

- ### `serverInfo`
    Grâce aux grandes capacités calculatoire de CampBot il peut donner des stats sur ce serveur.
