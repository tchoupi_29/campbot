FROM python:3.8

WORKDIR /campbot

COPY requirements.txt .
RUN pip install -r requirements.txt

CMD ["python", "main.py"]