import discord
import json
import datetime
from discord.ext import commands
from ..interfaces.views import PersistentViewButtonRecherche
from ..interfaces.select import RoleSelect
from ..utils import rolesEnCommun

class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_scheduled_event_update(self, before, after):
        if after.status == discord.EventStatus.active:
            view = discord.ui.View()
            liste = RoleSelect(after)
            # Add options
            options = ["WOT", "Enlisted", "WOWS", "WT", "Aucun"]
            for option in options:
                liste.add_option(label=option, value=option)

            with open("config/configRecherche.json", "r") as f:
                data = json.load(f)
            idSalon = int(data[str(after.guild.id)]["salon appeler les troupes"])
            channel = self.bot.get_channel(idSalon)
            view.add_item(liste)
            await channel.send(content="Quels rôle est ce que je dois spammer ?", view=view)

    @commands.Cog.listener()
    async def on_ready(self):
        self.bot.add_view(PersistentViewButtonRecherche())
        print("Le bot est co")


    @commands.Cog.listener()
    async def on_message(self, message):
        await self.bot.process_commands(message)


    @commands.Cog.listener()
    async def on_scheduled_event_update(self, before, after):
        if after.status == discord.EventStatus.active:
            view = discord.ui.View()
            liste = RoleSelect(after)
            liste.add_option(label="WOT", value="WOT")
            liste.add_option(label="Enlisted", value="Enlisted")
            liste.add_option(label="WOWS", value="WOWS")
            liste.add_option(label="WT", value="WT")
            liste.add_option(label="Aucun", value="Aucun")
            with open("config/configRecherche.json", "r") as f:
                data = json.load(f)
            idSalon = int(data[str(after.guild.id)]["salon appeler les troupes"])
            channel = self.bot.get_channel(idSalon)
            view.add_item(liste)
            await channel.send(content="Quels rôle est ce que je dois spammer ?", view=view)


    @commands.Cog.listener()
    async def on_presence_update(self, before, after: discord.Member):
        if after.guild.id != 1154356131494363157 or not rolesEnCommun(after, ["autoDispo"]):
            return

        # ajouter les roles de recherche
        with open("config/configRecherche.json", "r") as f:
            data = json.load(f)
            salonNotif = data[str(after.guild.id)]["salon de notif"]
            roleIds = data[str(after.guild.id)]["infos roles"].values()
        idRoleJeuxEnCours = []

        for activity in after.activities:
            if activity.name == "World of Tanks":
                idRoleJeuxEnCours.append(data[str(after.guild.id)]["infos roles"]["WOT"])
            elif activity.name == "War Thunder":
                idRoleJeuxEnCours.append(data[str(after.guild.id)]["infos roles"]["WT"])
            elif activity.name == "Enlisted":
                idRoleJeuxEnCours.append(data[str(after.guild.id)]["infos roles"]["Enlisted"])
            elif activity.name == "World of Warships":
                idRoleJeuxEnCours.append(data[str(after.guild.id)]["infos roles"]["WOWS"])

        if idRoleJeuxEnCours == []:
            # enlever personne dans le fichier json
            with open("storage/joueursDispo.json", "r") as f:
                data = json.load(f)
            for salon in data[str(after.guild.id)]:
                for msg in data[str(after.guild.id)][salon]:
                    if str(after.id) in data[str(after.guild.id)][str(salon)][str(msg)]:
                        with open("storage/joueursDispo.json", "w") as f:
                            data[str(after.guild.id)][str(salon)][str(msg)].pop(str(after.id))
                            json.dump(data, f)
                    
                    # enlever nom dans le msg du bot
                    originalMessage = await self.bot.get_channel(int(salon)).fetch_message(int(msg))
                    originalContent = originalMessage.content
                    newContent = ""
                    for elem in originalContent.split(after.mention + " "):
                        newContent += elem

                    # enlever les roles à la personne
                    rolesIDsInt = []
                    for roleID in roleIds:
                        rolesIDsInt.append(int(roleID))

                    for role in after.roles:
                        if role.id in rolesIDsInt:
                            await after.remove_roles(role)
                    await originalMessage.edit(content=newContent)
                return

        rolesAAjouter = []
        message = ""
        for roleId in idRoleJeuxEnCours:
            rolesAAjouter.append(after.guild.get_role(int(roleId)))
            await after.add_roles(rolesAAjouter[len(rolesAAjouter) - 1])
            message += rolesAAjouter[len(rolesAAjouter) - 1].name + " "

        # ajout du joueur dans le fichier json
        with open("storage/joueursDispo.json", "r") as f:
            data = json.load(f)
        ping = True
        for salon in data[str(after.guild.id)]:
            for msg in data[str(after.guild.id)][salon]:
                if not str(after.id) in data[str(after.guild.id)][salon][msg]:
                    with open("storage/joueursDispo.json", "w") as f:
                        data[str(after.guild.id)][salon][msg][str(after.id)] = str(datetime.datetime.now())  # datetime.timezone(datetime.timedelta(hours=2.0))
                        json.dump(data, f)

                    # ajouter le nom de la personne dans le msg du bot
                    originalMessage = await self.bot.get_channel(int(salon)).fetch_message(int(msg))
                    originalContent = originalMessage.content
                    newContent = ""
                    for jeu in originalContent.split("-"):
                        if not jeu.startswith("Voici"):
                            jeu = "-" + jeu
                        if jeu.split(" ")[1] in message:
                            if len(jeu.split(" ")) > 8:
                                ping = False
                            for elem in jeu.split(after.mention + " "):
                                newContent += elem
                            chaineList = list(newContent)
                            chaineList.insert(-2, after.mention + " ")
                            newContent = "".join(chaineList)
                        else:
                            newContent += jeu
                    await originalMessage.edit(content=newContent)
                else:
                    ping = False

        # envoie du ping dans le salon de notif
        if ping:
            content = ""
            for roleId in idRoleJeuxEnCours:
                content += after.guild.get_role(int(roleId)).mention + " "
            await self.bot.get_channel(int(salonNotif)).send(content + after.mention + " est dispo pour jouer à un jeu.")

async def setup(bot):
    await bot.add_cog(Events(bot))
