import random
from discord.ext import tasks, commands
import json
import discord
from copy import deepcopy
import datetime

class Tasks(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.changeActivity.start()
        self.checkGensDispoInJson.start()

    @tasks.loop(seconds=30.0)
    async def changeActivity(self):
        listeActivite = ["être beau", "wot", "wows", "enlisted", "appeler les troupes"]
        indActivite = random.randint(0, len(listeActivite) - 1)
        await self.bot.change_presence(activity=discord.Game(name=listeActivite[indActivite]))

    @tasks.loop(seconds=5.0)
    async def checkGensDispoInJson(self):
        with open("storage/joueursDispo.json", "r") as f:
            data = json.load(f)
            dataTmp = deepcopy(data)
        for idGuild, contenuGuild in data.items():
            for idSalon, contenuSalon in contenuGuild.items():
                for idMsg, contenuMsg in contenuSalon.items():
                    for idUSer, contenuUser in contenuMsg.items():
                        debutDeLADispo = datetime.datetime.strptime(contenuUser, "%Y-%m-%d %H:%M:%S.%f")
                        hourDebutDeLADispo = debutDeLADispo.hour + debutDeLADispo.minute / 60.0 + debutDeLADispo.second / 3600.0 + debutDeLADispo.microsecond / 3600000000.0
                        now = datetime.datetime.now() # datetime.timezone(datetime.timedelta(hours=2.0))
                        hourNow = now.hour + now.minute / 60.0 + now.second / 3600.0 + now.microsecond / 3600000000.0
                        if hourNow - hourDebutDeLADispo > 2:
                            # enlever personne dans le fichier json
                            with open("storage/joueursDispo.json", "w") as f:
                                dataTmp[idGuild][idSalon][idMsg].pop(idUSer)
                                json.dump(dataTmp, f)

                            # enlever nom dans le msg du bot
                            originalMessage = await self.bot.get_guild(int(idGuild)).get_channel(int(idSalon)).fetch_message(int(idMsg))
                            originalContent = originalMessage.content
                            newContent = ""
                            for elem in originalContent.split(self.bot.get_guild(int(idGuild)).get_member(int(idUSer)).mention + " "):
                                newContent += elem

                            # enlever les roles à la personne
                            with open("config/configRecherche.json", "r") as f:
                                data = json.load(f)
                            rolesIDs = data[str(idGuild)]["infos roles"]
                            rolesIDsInt = []
                            for roleID in rolesIDs.values():
                                rolesIDsInt.append(int(roleID))

                            for role in self.bot.get_guild(int(idGuild)).get_member(int(idUSer)).roles:
                                if role.id in rolesIDsInt:
                                    await self.bot.get_guild(int(idGuild)).get_member(int(idUSer)).remove_roles(role)
                            await originalMessage.edit(content=newContent)
    
    @changeActivity.before_loop
    @checkGensDispoInJson.before_loop
    async def beforeChangeActivity(self):
        await self.bot.wait_until_ready()

async def setup(bot):
    await bot.add_cog(Tasks(bot))
