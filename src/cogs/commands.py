import discord
import json
import typing
from discord.ext import commands
from ..interfaces.views import PersistentViewButtonRecherche
from ..utils import isAdmin, rolesEnCommun

class BotCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @discord.app_commands.command(name="init_recherche", description="Initialise le message du salon de recherche")
    @discord.app_commands.describe(channel_id="ID du salon dans lequel envoyer le message de recherche")
    @discord.app_commands.checks.has_permissions(administrator=True)
    async def init_recherche(self, interaction: discord.Interaction, channel_id: str):
        await interaction.response.defer(thinking=True)
        await interaction.delete_original_response()
        channel = self.bot.get_channel(int(channel_id))
        view = PersistentViewButtonRecherche()
        with open("config/configRecherche.json", "r") as f:
            dataConfig = json.load(f)
        contentJeu = ""
        for nomRole, idRole in dataConfig[str(interaction.guild_id)]["infos roles"].items():
            contentJeu += f"\n\u200B\n- {nomRole} "
        contentJeu += "\n\u200B\n\u200B"
        message = await channel.send(content=f"""Voici la liste des personnes dispos pour jouer en ce moment, si vous souhaitez vous aussi déclarer votre disponibilité vous pouvez cliquer sur le bouton 'Dispo'.{contentJeu}""", view=view)
        with open("storage/joueursDispo.json", "r") as f:
            data = json.load(f)
        with open("storage/joueursDispo.json", "w") as f:
            data[str(message.guild.id)] = {}
            data[str(message.guild.id)][channel_id] = {}
            data[str(message.guild.id)][channel_id][message.id] = {}
            json.dump(data, f)

    @discord.app_commands.command(name="server_infos", description="Donne des informations sur le serveur.")
    async def server_infos(self, interaction: discord.Interaction):
        await interaction.response.defer(thinking=True)
        server = interaction.guild
        numberOfTextChannels = len(server.text_channels)
        numberOfVoiceChannels = len(server.voice_channels)
        numberOfPerson = server.member_count
        message = f"Le serveur possède **{numberOfPerson} membres** et **{numberOfTextChannels} salons textuels** et **{numberOfVoiceChannels} salons vocaux**."
        await interaction.followup.send(message)

    @discord.app_commands.command(name="appeler_les_troupes",description="Je vais envoyer une invitation à tous les participants qui ont le rôle invitaion.")
    @discord.app_commands.describe(role="Rôle à appeler", ind_event="Indice de l'event dans la liste des events du serveur (0 par défaut)", id_invit_link_event_channel="ID du lien d'invit du salon de l'event (Brxg4UDH5hpar défaut)")
    async def appeler_les_troupes(self, interaction: discord.Interaction, role: typing.Literal["WOT", "WOWS", "Enlisted"], ind_event: int = 0, id_invit_link_event_channel: str = "Brxg4UDH5h"):
        await interaction.response.defer(thinking=True)
        if isAdmin(interaction):
            if len(interaction.guild.scheduled_events) > 0:
                if ind_event < len(interaction.guild.scheduled_events):
                    idEvent = interaction.guild.scheduled_events[ind_event].id
                    message = f"Cher participant du serveur Games Beyond History, je vous invite à participer à un événement qui se déroule dès maintenant. https://discord.gg/{id_invit_link_event_channel}?event={idEvent}"
                    gensSpamme = []
                    for member in interaction.guild.members:
                        nePasPing = False
                        rolesCommuns = rolesEnCommun(member, ["Staff", "Invitation", role])
                        if "Invitation" in rolesCommuns and role in rolesCommuns:
                            if "Staff" in rolesCommuns:
                                nePasPing = True
                            if nePasPing:
                                gensSpamme.append(member.name)
                            else:
                                gensSpamme.append(member.mention)

                            try:
                                await member.send(message)
                            except:
                                print(f"Le membre {member.name} a déclenché une erreur")
                    await interaction.followup.send(
                        f"J'ai eu le plaisir de spammer ces personnes : {' '.join(str(element) for element in gensSpamme)}")
                else:
                    await interaction.followup.send("Le numéro de l'event donné ne correspond à aucun event !")
            else:
                await interaction.followup.send("Aucun event n'a été créé sur ce serveur !")
        else:
            await interaction.followup.send("Vous n'êtes pas un admin !")


async def setup(bot):
    await bot.add_cog(BotCommands(bot))


