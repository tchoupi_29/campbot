import discord

def isAdmin(interaction: discord.Interaction):
    admin = False
    for role in interaction.user.roles:
        if role.name == "Staff" or role.name == "Officier":
            admin = True
    return admin


def rolesEnCommun(member, nomsRole):
    rolesEnCommun = []
    roles = [role.name for role in member.roles]
    for role in nomsRole:
        if role in roles:
            rolesEnCommun.append(role)
    return rolesEnCommun


async def appeler_les_troupes_auto(interaction: discord.Interaction, role, event):
    if isAdmin(interaction):
        if len(interaction.guild.scheduled_events) > 0:
            idEvent = event.id
            channel: discord.TextChannel = event.channel
            invites = await channel.invites()
            if len(invites) > 0:
                id_invit_link_event_channel = invites[0].id
            else:
                invite = await channel.create_invite()
                id_invit_link_event_channel = invite.id
            message = f"Cher participant du serveur Games Beyond History, je vous invite à participer à un événement qui se déroule dès maintenant. https://discord.gg/{id_invit_link_event_channel}?event={idEvent}"
            gensSpamme = []
            for member in interaction.guild.members:
                nePasPing = False
                rolesCommuns = rolesEnCommun(member, ["Staff", "Invitation", role])
                if "Invitation" in rolesCommuns and role in rolesCommuns:
                    if "Staff" in rolesCommuns:
                        nePasPing = True
                    if nePasPing:
                        gensSpamme.append(member.name)
                    else:
                        gensSpamme.append(member.mention)

                    try:
                        await member.send(message)
                    except:
                        print(f"Le membre {member.name} a déclenché une erreur")
            await interaction.followup.send(
                f"J'ai eu le plaisir de spammer les personnes avec le rôle {role} : {' '.join(str(element) for element in gensSpamme)}")
        else:
            await interaction.followup.send("Aucun event n'a été créé sur ce serveur !")
    else:
        await interaction.followup.send("Vous n'êtes pas un admin !")