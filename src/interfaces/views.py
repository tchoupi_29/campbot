import discord
import json
from .select import JeuxSelect

class PersistentViewButtonRecherche(discord.ui.View):
    def __init__(self):
        super().__init__(timeout=None)
        self.rolesIDs = None

    @discord.ui.button(style=discord.ButtonStyle.green, label="Dispo", custom_id="btnDispoCampBot")  # the button has a custom_id set
    async def buttonDispoCallback(self, interaction : discord.Interaction, button):
        await interaction.response.defer()
        if self.rolesIDs is None:
            with open("config/configRecherche.json", "r") as f:
                data = json.load(f)
            self.rolesIDs = data[str(interaction.message.guild.id)]["infos roles"]

        view = discord.ui.View(timeout=None)
        liste = JeuxSelect(infoMsgOrgin={"guildID": str(interaction.message.guild.id), "salonID": str(interaction.message.channel.id), "messageID": str(interaction.message.id)})
        for nomRole, idRole in self.rolesIDs.items():
            liste.add_option(label=nomRole, value=idRole)
        view.add_item(liste)
        await interaction.followup.send(f"Pour quels jeux êtes vous disponible ?", ephemeral=True, view=view)

    @discord.ui.button(style=discord.ButtonStyle.red, label="Plus dispo", custom_id="btnPasDispoCampBot")  # the button has a custom_id set
    async def buttonPasDispoCallback(self, interaction, button):
        await interaction.response.defer()
        if self.rolesIDs is None:
            with open("config/configRecherche.json", "r") as f:
                data = json.load(f)
            self.rolesIDs = data[str(interaction.message.guild.id)]["infos roles"]

        # enlever personne dans le fichier json
        with open("storage/joueursDispo.json", "r") as f:
            data = json.load(f)
        if str(interaction.user.id) in data[str(interaction.user.guild.id)][str(interaction.channel_id)][str(interaction.message.id)]:
            with open("storage/joueursDispo.json", "w") as f:
                data[str(interaction.user.guild.id)][str(interaction.channel_id)][str(interaction.message.id)].pop(str(interaction.user.id))
                json.dump(data, f)

        # enlever nom dans le msg du bot
        originalContent = interaction.message.content
        newContent = ""
        for elem in originalContent.split(interaction.user.mention + " "):
            newContent += elem

        # enlever les roles à la personne
        rolesIDsInt = []
        for roleID in self.rolesIDs.values():
            rolesIDsInt.append(int(roleID))

        for role in interaction.user.roles:
            if role.id in rolesIDsInt:
                await interaction.user.remove_roles(role)
        await interaction.edit_original_response(content=newContent)
