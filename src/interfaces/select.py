import discord
import datetime
import json
from ..utils import appeler_les_troupes_auto

class RoleSelect(discord.ui.Select):
    def __init__(self, event):
        self.event = event
        super().__init__(placeholder="Sélectionnez un rôle à ping")

    async def callback(self, interaction: discord.Interaction):
        await interaction.response.defer(thinking=True)
        if self.values[0] != "Aucun":
            await appeler_les_troupes_auto(interaction, self.values[0], self.event)
        else:
            await interaction.followup.send("Aucun membre ne se fera ping.")
        await interaction.message.edit(view=None)
        self.view.stop()


class JeuxSelect(discord.ui.Select):
    def __init__(self, infoMsgOrgin):
        super().__init__(placeholder="Choisissez un jeu ou plusieurs jeux :")
        self.infoMsgOrgin = infoMsgOrgin
        with open("config/configRecherche.json", "r") as f:
            dataConfig = json.load(f)
        jeuxPossibles = dataConfig[self.infoMsgOrgin["guildID"]]["infos roles"].keys()
        self.max_values = len(jeuxPossibles)
        self.salonNotif = dataConfig[self.infoMsgOrgin["guildID"]]["salon de notif"]

    async def callback(self, interaction: discord.Interaction):
        await interaction.response.defer()
        # ajout du joueur dans le fichier json
        with open("storage/joueursDispo.json", "r") as f:
            data = json.load(f)
        with open("storage/joueursDispo.json", "w") as f:
            data[str(interaction.user.guild.id)][self.infoMsgOrgin["salonID"]][self.infoMsgOrgin["messageID"]][str(interaction.user.id)] = str(datetime.datetime.now()) #datetime.timezone(datetime.timedelta(hours=2.0))
            json.dump(data, f)

        # ajouter les roles de recherche
        rolesAAjouter = []
        message = ""
        for roleId in self.values:
            rolesAAjouter.append(interaction.user.guild.get_role(int(roleId)))
            await interaction.user.add_roles(rolesAAjouter[len(rolesAAjouter) - 1])
            message += rolesAAjouter[len(rolesAAjouter) - 1].name + " "

        # ajouter le nom de la personne dans le msg du bot
        originalMessage = await interaction.client.get_channel(int(self.infoMsgOrgin["salonID"])).fetch_message(int(self.infoMsgOrgin["messageID"]))
        originalContent = originalMessage.content
        newContent = ""
        ping = True
        for jeu in originalContent.split("-"):
            if not jeu.startswith("Voici"):
                jeu = "-" + jeu
            if jeu.split(" ")[1] in message:
                if len(jeu.split(" ")) > 8:
                    ping = True  # à mettre sur false si jamais ça spam
                for elem in jeu.split(interaction.user.mention + " "):
                    newContent += elem
                chaineList = list(newContent)
                chaineList.insert(-2, interaction.user.mention + " ")
                newContent = "".join(chaineList)
            else:
                newContent += jeu
        await originalMessage.edit(content=newContent)

        # envoie du ping dans le salon de notif
        if ping:
            content = ""
            for roleId in self.values:
                content += interaction.user.guild.get_role(int(roleId)).mention + " "
            await interaction.client.get_channel(int(self.salonNotif)).send(content + interaction.user.mention + " est dispo pour jouer à un jeu.")

        await interaction.edit_original_response(content="Vous avez désormais les rôles : " + message, view=None)

