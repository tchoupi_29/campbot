import discord
import discord.ext.commands
import flask
import os
from functools import wraps
from dotenv import load_dotenv
import asyncio
import datetime

load_dotenv(dotenv_path="config/token")

app = flask.Flask(__name__)
bot: discord.ext.commands.bot = None

# # gbh
# SERVEUR_ID = 1154356131494363157
# SALON_EVENT_ID = 1155179500171182224
# SALON_ANNONCE_ID = 1154372368941191208

# dev
SERVEUR_ID = 971534426964131901
SALON_EVENT_ID = 971534427463229533
SALON_ANNONCE_ID = 1287123914962636800

def require_api_key(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        api_key = flask.request.headers.get('authorization')
        if api_key is None or api_key != os.getenv("TOKEN_CAMPBOT"):
            return flask.jsonify({'message': 'Unauthorized'}), 401
        return f(*args, **kwargs)
    return decorated_function

@app.get('/status')
def status():
    return "API is running!"

@app.post('/event/create')
@require_api_key
def create():
    params = flask.request.get_json(force=True)
    serv: discord.Guild = bot.get_guild(SERVEUR_ID)
    channel: discord.VoiceChannel = bot.get_channel(SALON_EVENT_ID)
    dateDebut = datetime.datetime.fromtimestamp(params["date"] / 1000, tz=datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo)
    titre = params["titre"]
    description = params["description"]
    message = params["message"]
    
    creerEventFuture = asyncio.run_coroutine_threadsafe(serv.create_scheduled_event(name=titre, start_time=dateDebut, channel=channel, description=description, privacy_level=discord.PrivacyLevel.guild_only), bot.loop)
    event = creerEventFuture.result()
    envoyerMsgFuture = asyncio.run_coroutine_threadsafe(serv.get_channel(SALON_ANNONCE_ID).send(message), bot.loop)
    msg = envoyerMsgFuture.result()
    
    eventId = event.id
    msgId = msg.id

    return flask.jsonify({"eventId": str(eventId), "msgId": str(msgId)})

@app.post('/event/update')
@require_api_key
def update():
    params = flask.request.get_json(force=True)
    serv: discord.Guild = bot.get_guild(SERVEUR_ID)
    channel: discord.TextChannel = bot.get_channel(SALON_ANNONCE_ID)
    idEvent = int(params["idEvent"])
    idMsg = int(params["idMsg"])
    dateDebut = datetime.datetime.fromtimestamp(params["infosEvent"]["date"] / 1000, tz=datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo)
    titre = params["infosEvent"]["titre"]
    description = params["infosEvent"]["description"]
    message = params["infosEvent"]["message"]

    eventDiscord = serv.get_scheduled_event(idEvent)
    updatedEventFuture = asyncio.run_coroutine_threadsafe(eventDiscord.edit(start_time=dateDebut, description=description, name=titre), bot.loop)
    event = updatedEventFuture.result()

    msgDiscord = channel.get_partial_message(idMsg)
    updatedMsgFuture = asyncio.run_coroutine_threadsafe(msgDiscord.edit(content=message), bot.loop)
    msg = updatedMsgFuture.result()

    return flask.jsonify(True)

@app.post('/event/delete')
@require_api_key
def delete():
    params = flask.request.get_json(force=True)
    serv: discord.Guild = bot.get_guild(SERVEUR_ID)
    channel: discord.TextChannel = bot.get_channel(SALON_ANNONCE_ID)
    idEvent = int(params["idEvent"])
    idMsg = int(params["idMsg"])

    eventDiscord = serv.get_scheduled_event(idEvent)
    deletedEventFuture = asyncio.run_coroutine_threadsafe(eventDiscord.delete(), bot.loop)
    event = deletedEventFuture.result()

    msgDiscord = channel.get_partial_message(idMsg)
    deletedMsgFuture = asyncio.run_coroutine_threadsafe(msgDiscord.delete(), bot.loop)
    msg = deletedMsgFuture.result()

    return flask.jsonify(True)

def setBot(botTmp):
    global bot
    bot = botTmp

def run_flask():
    app.run(port=5000, host="0.0.0.0") 